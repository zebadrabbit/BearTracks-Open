import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';
 
//const LocationsHistory = new Mongo.Collection('locations_history');
const Locations = new Mongo.Collection('locations');

import './main.html';

Router.route('/', function () {
	this.render('main');
});

Router.route('/logout', function() {
	Meteor.logout();
	this.render('main');
});

Meteor.subscribe("requests", {
    onReady: function () { console.log("onReady And the Items actually Arrive", arguments); },
    onError: function () { console.log("onError", arguments); }
});

Template.map.onRendered(function () {

	let markers = new L.layerGroup();

	let map = L.map('map', {
		//36.4815, 94.2733
		center: new L.LatLng(36.4815, -94.2733), 
		zoom: 5,
		loadingControl: true,
		layers: [markers]
	});

	L.tileLayer.provider('OpenStreetMap.Mapnik').addTo(map);
	//L.tileLayer('https://192.168.1.50/hot/{z}/{x}/{y}.png', {
    //  minZoom: 4,
	//	maxZoom: 16,
	//	attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
	//}).addTo(map);
	
	if (Meteor.userId() !== null) {

		Meteor.subscribe('locations');
		Meteor.subscribe('requests');

		let friends = new Object();

		let myIcon = L.icon({
			iconUrl: '/map-marker-128.png',
			iconSize: [48, 48],
			iconAnchor: [24, 48],
			popupAnchor: [0, -48],
		});

		let locationUpdates = Locations.find({});

		locationUpdates.observe({
			added: function(user_location) {
				friends[user_location.userId] = user_location;
				updateMarkers();
			},
			changed: function(user_location, old_user_location) {
                friends[user_location.userId] = user_location;
                updateMarkers();
            }
		});


		function updateMarkers() {
			if (friends) {
				markers.clearLayers();
				for (let k in friends) {
					L.marker([friends[k].latitude, friends[k].longitude], {icon: myIcon})
						.bindPopup(friends[k].userName)
						.addTo(markers)
						.on('click', function(e) {
							map.flyTo(e.latlng, 15);
						});
				}
			}
		}
		
		
		function onMapClick(e) {

			let locationData = {
				userId: Meteor.userId(),
				userName: Meteor.user().profile.name,
				createdAt: new Date(),
				latitude: e.latlng.lat,
				longitude: e.latlng.lng
			};

			Meteor.call('LocationsHistory.insert', locationData);
			Meteor.call('Locations.upsert', locationData);
			console.log('location update');
		}
		
		map.on('click', onMapClick);

	}

});

Template.main.events({
	
    'submit .register-form': function (event) {
 
        event.preventDefault();
 
        let email = event.target.email.value;
        let password = event.target.password.value;
        let firstname = event.target.firstname.value;
        let lastname = event.target.lastname.value;
 
        let user = {
        	email: email,
			password: password,
			profile: {
        		name: firstname + " " + lastname,
				picture: 'generic.png',
				friends: []
        	}
        };
 
        Accounts.createUser(user, function(err){
            if (!err) {
				$('#registerform').modal('hide');
                Router.go('/');
            }
        });
    },
	
	'submit .login-form': function (event) {
        
		event.preventDefault();
        
		let email = event.target.email.value;
        let password = event.target.password.value;
        
        Meteor.loginWithPassword(email, password, function(err){
            if (!err) {
				$('#loginform').modal('hide');
                Router.go('/');
            }
        });
    },

	'submit .profile-form': function(event) {

        event.preventDefault();

	},

	'submit .friends-form': function(event) {

        event.preventDefault();

        let targetEmail = event.target.email.value;
        if (!Meteor.user().profile.friends.includes(targetEmail)) {
        	Meteor.call('addRequest', targetEmail);
        }

	}
	
});
