'use strict';

var winston = require('winston');

import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

const LocationsHistory = new Mongo.Collection('locations_history');
const Locations = new Mongo.Collection('locations');
const Requests = new Mongo.Collection('requests');



Meteor.startup(() => {
    var winston = require('winston');

    //let logDir = Meteor.settings['private']['logs'];

    let logOpts = {
        transports: [
            new (winston.transports.Console)(),
            new (winston.transports.File)({ filename: 'somefile.log' })
        ]
    };

    let logger = new winston.Logger(logOpts);

    let log = console.log;
    console.log = function() {
        if (arguments.length === 1 && arguments[0] === 'LISTENING') {
            return log.call(console, 'LISTENING');
        }
        logger.info.apply(logger, arguments);
    };

    ['info', 'warn', 'error', 'debug'].forEach(function(method) {
        console[method] = function() {
            logger[method].apply(logger, arguments);
        }
    });

});

// Deny all client-side updates to user documents
Meteor.users.deny({
    update() { return true; }
});

Meteor.publish('locations', function() {
    return Locations.find({
        $or: [
            {
                userId: this.userId
            },
            {
                friends: [ this.userId ]
            }
        ]
    });
});

//  {
//      parent: <userId:string>,
//      target: <userId:string>
//  }

Meteor.publish('requests', function() {
    return Requests.find( {
        target: this.userId
    });
});

Meteor.methods({

    'Locations.upsert' (location) {
        Locations.upsert({
            userId: location.userId
        }, {
            $set: {
                userName: location.userName,
                createdAt: new Date(),
                latitude: location.latitude,
                longitude: location.longitude
            }
        });

    },

    'LocationsHistory.insert' (location) {
		if (this.userId !== null) {
			LocationsHistory.insert(location);
		}
    },


    'addRequest' (target) {
        Requests.insert({
            parent: this.userId,
            target: target
        });
    },

    'confirmRequest' (target) {
        let request = Requests.find({
            target: target
        });

        if (request !== null) {
            Meteor.call('addFriend', request.target, (err, result) => {
                if (!err) {
                    Meteor.call('denyRequest', request.target);
                }
            });
        }

    },

    'denyRequest' (target) {
        Requests.delete({
            parent: this.userId,
            target: target
        })
    },


    'addFriend' (userId) {
        if (this.userId !== null) {
            if (!Meteor.user().profile.friends.includes(userId)) {
                Meteor.users.update({
                    _id: this.userId
                }, {
                    $push: {
                        'profile.friends': userId
                    }
                });
                console.log('Added friend');
            }
        }
    },

    'delFriend' (userId) {
        if (this.userId !== null) {
            if (!Meteor.user().profile.friends.includes(userId)) {
                Meteor.users.update({
                    _id: this.userId
                }, {
                    $pull: {
                        'profile.friends': userId
                    }
                });
                console.log('Deleted friend');
            }
        }
    },

    'findFriend' (byEmail) {
        if (this.userId !== null) {
            let account = Accounts.findUserByEmail(byEmail);
            if (account) {
                console.log('Found friend');
                return account._id;
            }
        }
    }

});

